import Ember from 'ember';

export default Ember.View.extend({
	_alphaClick: function(){
		$('.atoz-link').on('click', function(ev){
			ev.preventDefault();
			var toTarget = ev.currentTarget.hash;
			$('html, body').animate({
				scrollTop: $(toTarget).offset().top - 100
			}, 500)
		});
	}.on('didInsertElement')
});