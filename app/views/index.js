import Ember from 'ember';

export default Ember.View.extend({
	getSlideIndex: function() {
		var currentPath = this.controller.currentPath;
		if (currentPath === 'index.demand') {
			return 0;
		}
		if (currentPath === 'index.soon') {
			return 2;
		}
		// Default:'index.index'
		return 1;
	},
	onAfterSlideChange: function(index) {
		var nav = $('.state-nav img');
		switch(index){
			case 0 :
				$('.state-nav li.active').removeClass('active');
				$('.state-nav li.on-demand').addClass('active');
				break;
			case 1 :
				$('.state-nav li.active').removeClass('active');
				$('.state-nav li.on-four').addClass('active');		
				break;
			case 2 :
				$('.state-nav li.active').removeClass('active');
				$('.state-nav li.on-soon').addClass('active');
				break;
		};
	},

	_initHomepageNavigation: function(){

		var isTablet = Modernizr.touch;

		console.log( 'isTablet ', isTablet );

		// $(window).on('scroll touchstart', function(){

		// 	var $nav = $('.state-nav');
		// 	var globalNavPos = $('.navbar').position('top');

		// 	var stateNavOffset = isTablet ? 0 : 97;
		// 	var toptop = isTablet ? '60px' : '188px';


		// 	var windowTop = $(window).scrollTop();

		// 	if (windowTop > stateNavOffset) {
		// 		if ($nav.data('size') == 'big') {
		// 			$nav.data('size','small').stop().animate({
		// 				height:'60px'
		// 			}, 600).removeClass('big').addClass('small')
		// 		}
		// 		if(!isTablet){
		// 			$nav.css('top', windowTop+68)
		// 		}
		// 	} else {
		// 		if ($nav.data('size') == 'small') {
		// 			$nav.data('size','big').stop().animate({
		// 				height:'97px'
		// 			}, 600).css('top', toptop).removeClass('small').addClass('big')
		// 		}
		// 	}
		// });
	},

	didInsertElement: function() {
		this._initHomepageNavigation();
		var self = this;
		var slideIndex = this.getSlideIndex();
		self.onAfterSlideChange(slideIndex);
		
		var slick = $('.homepage-states').slick({
			infinite: false,
			arrows: false,
			slidesToShow: 1,
			touchMove: false,
			onInit: function(s){
				//s.currentSlide = slideIndex;
				//console.log(s);
				//s.setPosition(0);
				//s.slickGoTo(slideIndex);
			},
			onAfterChange: function(t, i){
				self.onAfterSlideChange(i);
			}
		});
		slick.slickGoTo(slideIndex);
	},
	onCurrentPathChanged: function(){
		var slideIndex = this.getSlideIndex();
		window.scrollTo(0,0);
		$('.homepage-states').slickGoTo(slideIndex);
	}.observes('controller.currentPath')
});