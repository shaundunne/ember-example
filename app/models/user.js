import DS from "ember-data";

// 'signed-in', 'signed-in-notification', 'signed-in-no-notification'
// 'first-time-use', 'notification-use', 'regular-use'

export default DS.Model.extend({
	firstName: DS.attr('string'),
	lastName: DS.attr('string'),
	type: DS.attr('string', {defaultValue: 'first-time-use'}),
	fullName: function() {
		return this.get('firstName') + ' ' + this.get('lastName');
	}.property('firstName', 'lastName')
});