import Ember from 'ember';
export default Ember.Component.extend({
	layoutName: 'components/bh-carousel',
	startIndex: 0,
	listenTo: null,
	actions: {
		handlePlayMeClicked: function() {
			this.sendAction('playClicked');
		},
		handleRemindMeClicked: function() {
			this.sendAction('triggerRemindMe');
		}
	},
	didInsertElement: function(){
		var self = this;

		var numSlides = 3;
		var arrows = true;
		var centerPadding = '0px';

		if( Modernizr.mq('screen and (min-width : 200px) and (max-width : 641px)') ) {
			numSlides = 1;
			arrows = false;
			centerPadding = '20px';
		}

		$('.brandhub-carousel').slick({
			slidesToShow: numSlides,
			centerMode: false,
			centerPadding: centerPadding,
			arrows: arrows,
			infinite: false,
			onAfterChange: function(event) {
				self.sendAction('slideChanged', event.currentSlide);
			}
		});

		var startIndex = this.get('startIndex');
		$('.brandhub-carousel').slickGoTo(startIndex);

		var listenTo = self.get('listenTo');
		if (listenTo) {
			listenTo.on('selectSeries', this.onSelectSeries);
			listenTo.on('selectCarouselRemindMeButton', this.onSelectCarouselRemindMeButton);
		};
		
	},
	onSelectSeries: function(series) {
		//console.log('on select series');
		var items = this.get('model');

		for(var i = 0; i < items.length; i++) {
			if (items[i].series == series) {
				$('.brandhub-carousel').slickGoTo(i);
				break;
			}
		}
	},
	onSelectCarouselRemindMeButton: function() {
		var $btn = $('.js-bh-item-remind-btn').last();

		if ($btn) {
			if ($btn.hasClass('btn-is-selected')) {
				$btn.removeClass('btn-is-selected');
			} else {
				$btn.addClass('btn-is-selected');
			}
		}
	}
});