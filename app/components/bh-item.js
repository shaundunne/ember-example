import Ember from 'ember';
export default Ember.Component.extend({
	tagName: 'div',
	classNames: ['episode-item'],
	title: '',
	date: '',
	thumbnail: '',
	click: function(e) {
		var remindable = this.get('remindable');
		if (remindable) {
			//console.log('remindClicked');
			this.sendAction('remindMeClicked');
		} else {
			//console.log('playClicked');
			this.sendAction('playMeClicked');
		}
	}
});