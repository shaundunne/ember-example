import Ember from 'ember';
export default Ember.Component.extend({
	layoutName: 'components/search-box',
	isListOpen: false,
	globalNavDropdownOpen: false, 
	classNameBindings: ['isListOpen:is-open:is-closed'],
	resetSearchBox: function() {
		this.set('isListOpen', false);
		$('#searchInput').val('');
	},
	actions: {
		resultsClicked: function() {
			this.resetSearchBox();
		}
	},
	didInsertElement: function() {
		var self = this;
		self.set('isListOpen', false);

		var searchInput = $('#searchInput');

        var query = 'made in chelsea';

		searchInput.keyup(function() {
			var str = $(this).val().toLowerCase();

			if (query.indexOf(str) != -1) {
				self.set('isListOpen', true);
			} else {
				self.set('isListOpen', false);
			}

			if (str.length == 0) {
				self.set('isListOpen', false);
			}
		});
	},
	globalNavDropdownChanged: function(){
		this.resetSearchBox();
	}.observes('globalNavDropdownOpen')
});