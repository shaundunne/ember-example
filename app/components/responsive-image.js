import Ember from 'ember';
export default Ember.Component.extend({
	layoutName: 'components/responsive-image',
	src: '',
	imgSrc: '',
	className: 'responsive-image',
	didInsertElement: function() {
		var src = this.get('src');
		var srcSet = src.split(',');

		//mobile, tablet, desktop

		var path = srcSet[0];
		if( Modernizr.mq('screen and (min-width : 641px) and (max-width : 1224px)') ) {
			path = srcSet[1];
		}
		if( Modernizr.mq('screen and (min-width : 1225px)') ) {
			path = srcSet[2];
		}
		
		this.set('imgSrc', path.replace(/ /g,''));

		var className = this.get('className');
		this.set('className', 'responsive-image ' + className);
	}
});