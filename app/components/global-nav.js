import Ember from 'ember';
export default Ember.Component.extend({
	layoutName: 'components/global-nav',
	oldActiveListItem: null,
	isDropdownOpen: false,
	isFixed: false,
	userWithNotification: false,
	controller : null,
	actions: {
		toggleDropdown: function() {
			var isOpen = this.get('isDropdownOpen');
            this.set('isDropdownOpen', !isOpen);
            //Quick hack - could probably be better, but, whatever. - Shaun
            window.scrollTo(0, 0)
		},
		signInClicked: function() {
			this.sendAction('signInClicked');
		},
		signOutClicked: function() {
			this.sendAction('signOutClicked');
		},
		accountSettingsClicked: function() {
			this.sendAction('accountSettingsClicked');
		}
	},
	didInsertElement: function() {
		this.invalidateUserIcon();

		// http://jsfiddle.net/cc48t/

		var self = this;
		var $navbar = $('.js-navbar');

		if( Modernizr.mq('screen and (min-width : 1225px)') ) {

			var yOffset = 120;
			$(window).scroll(function () {

				var sTop = $(window).scrollTop();
			    if (sTop > yOffset) {
			    	//$('#main').css('padding-top', '68px');
			    	//self.set('isFixed', true);

			        $navbar.css({
                        'top' :sTop - yOffset

                    });
			    } else {
			    	//self.set('isFixed', false);
			    	$navbar.css('top', 'auto');
			    }
			});

		} else {
			$('body').css('margin-top', $navbar.height() + 'px');
			self.set('isFixed', true);
		}

		// Reset discover dropdown
		$('.js-navbar-listitem').click(function() {
			self.set('isDropdownOpen', false);
		});
	},
	dropdownChanged: function() {
		var oldActiveListItem = this.get('oldActiveListItem');

		if (oldActiveListItem) {
			oldActiveListItem.addClass('active');
			this.set('oldActiveListItem', null);
		} else {
			var listItem = $('.js-navbar-listitem.active');
			if (listItem.length !== 0) {
				listItem.removeClass('active');
				this.set('oldActiveListItem', listItem);
			}
		}
	}.observes('isDropdownOpen'),
	userTypeChanged: function() {
		this.invalidateUserIcon();
	}.observes('user.type'),
	invalidateUserIcon: function() {
		var user = this.get('user');
		if (user) {
			var type = user.get('type');
			
			if (type === 'notification-use') {
				this.set('userWithNotification', true);
			} else {
				this.set('userWithNotification', false);
			}
		} else {
			this.set('userWithNotification', false);
		}
	}
});