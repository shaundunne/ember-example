import Ember from 'ember';

export default Ember.Component.extend({
	actions: {
		submit: function() {
			this.sendAction('signIn')
		},
		cancel: function() {
			this.sendAction('cancel');
		}
	}
});