import Ember from 'ember';
export default Ember.Component.extend({
	dataProvider: [],
	controller: null,
	min: 1,
	max: 1,
	seriesVal: 1,
	classNames: ['bh-paginator'],
	actions: {
		itemClicked: function(item) {
			this.set('seriesVal', item);
			this.invalidatePaginator();

			this.sendAction('itemClicked', item);
		}
	},

	setupFlags: function() {
		var min = this.get('min');
		var max = this.get('max');
		var arr = [];
		for (var i = min; i <= max; i++) {
			arr.push(i);
		}
		this.set('dataProvider', arr);
	}.on('init'),

	invalidatePaginator: function() {
		var seriesVal = this.get('seriesVal');
		var $listItem = $('.js-bh-paginator-list-item');
		$listItem.removeClass('active');

		$listItem.eq(seriesVal - 1).addClass('active');
	}.on('didInsertElement'),
	
	seriesValChanged: function(){
		this.invalidatePaginator();
	}.observes('seriesVal')
});