import Ember from 'ember';
export default Ember.Component.extend({
    layoutName: 'components/epg-playbar',
    onScroll: function(event) {

        if( Modernizr.mq('screen and (max-width : 1225px)') ) {
            return;
        }

        var $window = event.currentTarget;
        var scrollY = $window.scrollY;
        var outerHeight = $window.outerHeight;
        var innerHeight = $window.innerHeight;

        var y = scrollY + innerHeight;

        var $epgPlaybarContainer = $('.js-epg-playbar-container');

        //var playbarTop = 1270;
        var playbarTop = ($('.js-epg-helper').offset().top + 135);

        if (y > playbarTop) {
            $epgPlaybarContainer.css('position', 'relative');
        } else {
            $epgPlaybarContainer.css('position', 'fixed');
        }
    },
    didInsertElement: function() {
        var top = $('.js-epg-playbar-container').offset().top;
        //this.set('playbarTop', top);

        $(window).bind('scroll', this.onScroll);
    },
    willDestroyElement: function() {
        $(window).unbind('scroll', this.onScroll);
    }
});