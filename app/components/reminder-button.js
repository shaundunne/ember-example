import Ember from 'ember';
export default Ember.Component.extend({
    tagName: 'button',
    classNameBindings: 'isReminder',
    classNames: ['action-btn','action-reminder'],
    isFavourite: false,
    user: null,
    listenTo: null,
    title: 'fdfd',
    didInsertElement: function() {
        var listenTo = this.get('listenTo');
        if (listenTo) {
            listenTo.on('triggerRemindMe', this, this.onTriggerRemindMe);
        }
    },
    onTriggerRemindMe: function() {
        this.click();
    },
    click: function(){
        var user = this.get('user');

        if (user) {
            window.scrollTo(0,0)
            var isRem = this.get('isReminder');
            if(isRem){
                this.set('isReminder', false)
            } else {
                this.showPopup();
            }

            var listenTo = this.get('listenTo');
            if (listenTo) {
                listenTo.trigger('selectCarouselRemindMeButton');
            }
        }

        this.sendAction('clicked');
    },
    showPopup: function(){
        var popup = $('<div class="overlay"></div><img class="favourite-popup" src="assets/img/buttons/reminder-popup.png">');
        var self = this;
        popup.appendTo('body');
        popup.on('click', function(){
            popup.remove();
            self.set('isReminder', true);
            self.notify();
        })
    },
    notify: function(){
        var notification = $('<img class="notification-reminder" src="assets/img/buttons/reminder-finger.png">');
        var self = this;
        notification.appendTo('body');
        notification.animate({
                right: 0
            }, 600, function(){
            setTimeout(function(){
                notification.fadeOut()
            }, 3000)
        });
    }
});