import Ember from 'ember';
export default Ember.Component.extend({
	tagName: 'div',
	classNames: ['notification-dropdown'],
	hasNotification: false,
	click: function(){
		this.sendAction('clicked')
	}
});