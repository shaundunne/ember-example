import Ember from 'ember';
export default Ember.Component.extend({
  tagName: 'button',
  classNameBindings: 'isFavourite',
  classNames: ['action-btn','action-favourite'],
  isFavourite: false,
  user: null,
  click: function(){
    var user = this.get('user');

    if (user) {
      var isFav = this.get('isFavourite');
      window.scrollTo(0,0);
      if(isFav){
        this.set('isFavourite', false)
        console.log('UnFav');
      } else {
        this.showPopup();
        console.log('Favourited');
      }
    }
    
    this.sendAction('clicked');
  },
  showPopup: function(){
    var popup = $('<div class="overlay"></div><img class="favourite-popup" src="assets/img/buttons/favourite-popup.png">');
    var self = this;
    popup.appendTo('body');
    popup.on('click', function(){
      popup.remove();
      self.set('isFavourite', true);
      self.notify();
    })
  },
  notify: function(){
    var notification = $('<img class="notification-favourite" src="assets/img/buttons/favourite-finger.png">');
    var self = this;
    notification.appendTo('body');
    notification.animate({
      right: 0
    }, 600, function(){
      setTimeout(function(){
        notification.fadeOut()
      }, 3000)
    });
  }
});