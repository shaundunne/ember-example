import Ember from 'ember';

var Router = Ember.Router.extend({
  location: 'history'
});

Router.map(function() {
    this.resource('index', {path: '/'}, function () {
        this.route('demand');
        this.route('soon');
    });

    this.resource('myfour', function() {
        this.route('settings');
    });
    
    this.route('epg');
    this.resource('categories', function() {
    	this.route('az', { path: 'az/:alpha' });
    	this.route('comedy');
        this.route('entertainment');
        this.route('drama');
        this.route('factual');
        this.route('lifestyle');
        this.route('sport');
        this.route('ad');
    });
    //this.resource('categories', { path: '/categories/:category_slug' });
    this.route('shorts');
    this.route('catchup', {path: 'catchup'});

    //Utopia
    this.route('utopia');
    this.route('utopia-player');
    this.route('utopia-player-landscape');

    //Simulcast
    this.route('simulcast-player');
    this.route('simulcast-player-landscape');

    //Mic
    this.route('mic'); //Made in Chelsea
    this.route('mic-player');
    this.route('mic-player-landscape');

    this.route('micGallery', { path: 'mic/gallery'});

    this.route('styleguide');
    this.route('e4');
});

export default Router;
