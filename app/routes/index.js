import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);

		var controller = this.controllerFor('application');
		controller.trigger('enableHomeScroll');
	},
	deactivate: function() {
		var controller = this.controllerFor('application');
		controller.trigger('disableHomeScroll');
	},
	controllerName: 'application',
	model: function(){
		return ['red', 'yellow', 'blue', 'green'];
	}
});
