import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	model: function() {
		var arr = [
			{
				series: 1,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '9 May 2011',
				metadata: 's1/s1e1_episode_metadata.png',
				episodes: 's1/s1e1_clips_shorts.png'
			},
			{
				series: 1,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '16 May 2011',
				metadata: 's1/s1e2_episode_metadata.png',
				episodes: 's1/s1e2_clips_shorts.png'
			},
			{
				series: 1,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '23 May 2011',
				metadata: 's1/s1e3_episode_metadata.png',
				episodes: 's1/s1e3_clips_shorts.png'
			},
			{
				series: 1,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '30 May 2011',
				metadata: 's1/s1e4_episode_metadata.png',
				episodes: 's1/s1e4_clips_shorts.png'
			},
			{
				series: 1,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '6 Jun 2011',
				metadata: 's1/s1e5_episode_metadata.png',
				episodes: 's1/s1e5_clips_shorts.png'
			},
			{
				series: 2,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '19 Sep 2011',
				metadata: 's2/s2e1_episode_metadata.png',
				episodes: 's2/s2e1_clips_shorts.png'
			},
			{
				series: 2,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '26 Sep 2011',
				metadata: 's2/s2e2_episode_metadata.png',
				episodes: 's2/s2e2_clips_shorts.png'
			},
			{
				series: 2,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '3 Oct 2011',
				metadata: 's2/s2e3_episode_metadata.png',
				episodes: 's2/s2e3_clips_shorts.png'
			},
			{
				series: 2,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '10 Oct 2011',
				metadata: 's2/s2e4_episode_metadata.png',
				episodes: 's2/s2e4_clips_shorts.png'
			},
			{
				series: 2,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '17 Oct 2011',
				metadata: 's2/s2e5_episode_metadata.png',
				episodes: 's2/s2e5_clips_shorts.png'
			},
			{
				series: 3,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '2 Apr 2012',
				metadata: 's3/s3e1_episode_metadata.png',
				episodes: 's3/s3e1_clips_shorts.png'
			},
			{
				series: 3,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '9 Apr 2012',
				metadata: 's3/s3e2_episode_metadata.png',
				episodes: 's3/s3e2_clips_shorts.png'
			},
			{
				series: 3,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '16 Apr 2012',
				metadata: 's3/s3e3_episode_metadata.png',
				episodes: 's3/s3e3_clips_shorts.png'
			},
			{
				series: 3,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '23 Apr 2012',
				metadata: 's3/s3e4_episode_metadata.png',
				episodes: 's3/s3e4_clips_shorts.png'
			},
			{
				series: 3,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '30 Apr 2012',
				metadata: 's3/s3e5_episode_metadata.png',
				episodes: 's3/s3e5_clips_shorts.png'
			},
			{
				series: 4,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '15 Oct 2012',
				metadata: 's4/s4e1_episode_metadata.png',
				episodes: 's4/s4e1_clips_shorts.png'
			},
			{
				series: 4,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '22 Oct 2012',
				metadata: 's4/s4e2_episode_metadata.png',
				episodes: 's4/s4e2_clips_shorts.png'
			},
			{
				series: 4,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '29 Oct 2012',
				metadata: 's4/s4e3_episode_metadata.png',
				episodes: 's4/s4e3_clips_shorts.png'
			},
			{
				series: 4,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '5 Nov 2012',
				metadata: 's4/s4e4_episode_metadata.png',
				episodes: 's4/s4e4_clips_shorts.png'
			},
			{
				series: 4,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '12 Nov 2012',
				metadata: 's4/s4e5_episode_metadata.png',
				episodes: 's4/s4e5_clips_shorts.png'
			},
			{
				series: 5,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '8 Apr 2013',
				metadata: 's5/s5e1_episode_metadata.png',
				episodes: 's5/s5e1_clips_shorts.png'
			},
			{
				series: 5,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '15 Apr 2013',
				metadata: 's5/s5e2_episode_metadata.png',
				episodes: 's5/s5e2_clips_shorts.png'
			},
			{
				series: 5,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '22 Apr 2013',
				metadata: 's5/s5e3_episode_metadata.png',
				episodes: 's5/s5e3_clips_shorts.png'
			},
			{
				series: 5,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '29 Apr 2013',
				metadata: 's5/s5e4_episode_metadata.png',
				episodes: 's5/s5e4_clips_shorts.png'
			},
			{
				series: 5,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '6 May 2013',
				metadata: 's5/s5e5_episode_metadata.png',
				episodes: 's5/s5e5_clips_shorts.png'
			},
			{
				series: 6,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '14 Oct 2013',
				metadata: 's6/s6e1_episode_metadata.png',
				episodes: 's6/s6e1_clips_shorts.png'
			},
			{
				series: 6,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '21 Oct 2013',
				metadata: 's6/s6e2_episode_metadata.png',
				episodes: 's6/s6e2_clips_shorts.png'
			},
			{
				series: 6,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '28 Oct 2013',
				metadata: 's6/s6e3_episode_metadata.png',
				episodes: 's6/s6e3_clips_shorts.png'
			},
			{
				series: 6,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '4 Nov 2013',
				metadata: 's6/s6e4_episode_metadata.png',
				episodes: 's6/s6e4_clips_shorts.png'
			},
			{
				series: 6,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '11 Nov 2013',
				metadata: 's6/s6e5_episode_metadata.png',
				episodes: 's6/s6e5_clips_shorts.png'
			},
			{
				series: 7,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '7 Apr 2014',
				metadata: 's7/s7e1_episode_metadata.png',
				episodes: 's7/s7e1_clips_shorts.png'
			},
			{
				series: 7,
				episode: 2,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '14 Apr 2014',
				metadata: 's7/s7e2_episode_metadata.png',
				episodes: 's7/s7e2_clips_shorts.png'
			},
			{
				series: 7,
				episode: 3,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '21 Apr 2014',
				metadata: 's7/s7e3_episode_metadata.png',
				episodes: 's7/s7e3_clips_shorts.png'
			},
			{
				series: 7,
				episode: 4,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '28 Apr 2014',
				metadata: 's7/s7e4_episode_metadata.png',
				episodes: 's7/s7e4_clips_shorts.png'
			},
			{
				series: 7,
				episode: 5,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '5 May 2014',
				metadata: 's7/s7e5_episode_metadata.png',
				episodes: 's7/s7e5_clips_shorts.png'
			},
			{
				series: 7,
				episode: 6,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '12 May 2014',
				metadata: 's7/s7e6_episode_metadata.png',
				episodes: 's7/s7e6_clips_shorts.png'
			},
			{
				series: 7,
				episode: 7,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '19 May 2014',
				metadata: 's7/s7e7_episode_metadata.png',
				episodes: 's7/s7e7_clips_shorts.png'
			},
			{
				series: 7,
				episode: 8,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '26 May 2014',
				metadata: 's7/s7e8_episode_metadata.png',
				episodes: 's7/s7e8_clips_shorts.png'
			},
			{
				series: 7,
				episode: 9,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '2 Jun 2014',
				metadata: 's7/s7e9_episode_metadata.png',
				episodes: 's7/s7e9_clips_shorts.png'
			},
			{
				series: 7,
				episode: 10,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '9 Jun 2014',
				metadata: 's7/s7e10_episode_metadata.png',
				episodes: 's7/s7e10_clips_shorts.png'
			},
			{
				series: 7,
				episode: 11,
				thumbnail: '/assets/img/mic-hub/e.jpg',
				date: '16 Jun 2014',
				metadata: 's7/s7e11_episode_metadata.png',
				episodes: 's7/s7e11_clips_shorts.png'
			},
			{
				series: 8,
				episode: 1,
				thumbnail: '/assets/img/mic-hub/s8e1_episode_img.png',
				date: '16 Aug 2014',
				metadata: 's8/s8e1_episode_metadata.png',
				episodes: 's8/s8e1_clips_shorts.png',
				remindable: true
			}
		];

        if ( Modernizr.mq('screen and (min-width : 1000px)') ) {
            arr.push( {}, {} );
        };
        return arr;
	},
	setupController: function(controller, model) {
		var user = this.controllerFor('application').get('user');
		controller.set('model', model);
		controller.set('user', user);
		controller.set('episodeIndex', model.length-4);
	},
	actions: {
		gotoPlayer: function() {
			this.transitionTo('mic-player');
		},
		seriesItemClicked: function(index) {
			var controller = this.controllerFor('mic');
			controller.trigger('selectSeries', index);
		},
		triggerRemindMe: function() {
			console.log('trigger Me');
			var controller = this.controllerFor('mic');
			controller.trigger('triggerRemindMe');
		}
	}
});