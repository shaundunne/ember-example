import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	controllerName: 'player-default',
	actions: {
		didTransition: function() {
			this.controllerFor('mic-player').send('resetPlayer');
		},
		launchLandscapePlayer: function() {
			this.transitionTo('mic-player-landscape');
		}
	}
});
