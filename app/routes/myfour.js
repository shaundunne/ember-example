import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	setupController: function(controller, model) {
		var user = this.controllerFor('application').get('user');
		controller.set('model', user);
		controller.invalidateUserState();
	}
});
