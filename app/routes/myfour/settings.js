import Ember from 'ember';
import ResetScroll from '../../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	beforeModel: function() {
		var applicationController = this.controllerFor('application');
		var user = applicationController.get('user');

		if (!user) {
			this.transitionTo('myfour');
			applicationController.openLogin();
		}
	}
});
