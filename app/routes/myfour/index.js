import Ember from 'ember';
import ResetScroll from '../../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	controllerName: 'myfour',
	activate: function() {
		this._super.apply(this, arguments);
	}
});
