import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	actions: {
		didTransition: function() {
			this.controllerFor('application').send('resetDropdown');
			this.controllerFor('epg').send('resetCalendarDropdown');
		}
	}
});

