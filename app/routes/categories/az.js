import Ember from 'ember';
import ResetScroll from '../../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	model: function(params){
		var programs = 
		function(){
			console.log('message');
		};
		return {
			"section": params.alpha,
			"categories": [
				{
					'letter': 'a',
					'programmes': [
						{
							'title': 'A Better Life',
							'meta' : '5 Episodes'
						},
						{
							'title': 'A Bipolar Expedition',
							'meta' : '7 Episodes' 
						},
						{
							'title': 'A Boy Called Alex',
							'meta' : '4 Episodes' 
						},
						{
							'title': 'A Boy Called Alex: The Concert',
							'meta' : '1 Episode' 
						},
						{
							'title': 'A Challenge for Robin Hood',
							'meta' : '3 Episodes' 
						},
						{
							'title': 'A Civil Action',
							'meta' : '1 Episode' 
						},
						{
							'title': 'A Field in England',
							'meta' : '12 Episodes' 
						},
						{
							'title': 'A Great British Air Disaster',
							'meta' : '2 Episodes' 
						},
						{
							'title': 'A Place in the Sun',
							'meta' : '2 Episodes' 
						},
						{
							'title': 'A Place in the Sun Down Under',
							'meta' : '13 Episodes' 
						},
						{
							'title': 'A Prophet',
							'meta' : '3 Episodes' 
						},
						{
							'title': 'A Running Jump',
							'meta' : '1 Episode' 
						}
					]
				},
				{
					'letter': 'b',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'c',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'd',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'e',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'f',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'g',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'h',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'i',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'j',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'k',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'l',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'm',
					'programmes': [
						{
							'title': 'MacGruber',
							'meta' : '1 Episodes'
						},
						{
							'title': 'Machan',
							'meta' : '1 Episodes'
						},
						{
							'title': 'Machete',
							'meta' : '1 Episodes'
						},
						{
							'title': 'Machinist',
							'meta' : '2 Episodes'
						},
						{
							'title': 'The Mad Bad Ad Show',
							'meta' : '4 Episodes'
						},
						{
							'title': 'Mad Hot Ballroom',
							'meta' : '6 Episodes'
						},
						{
							'title': 'Mad on Chelsea',
							'meta' : '5 Episodes'
						},
						{
							'title': 'Made in Chelsea',
							'meta' : '7 Episodes',
							'url'	 : 'mic'
						},
						{
							'title': 'Made in England',
							'meta' : '3 Episodes'
						},
						{
							'title': 'Made in Italy: Top 10  Classic Dishes',
							'meta' : '2 Episodes'
						},
						{
							'title': 'Madeleine Was Here',
							'meta' : '1 Episodes'
						},
						{
							'title': 'The Madness of Boy George',
							'meta' : '1 Episodes'
						},
						{
							'title': 'Mafia Hunters',
							'meta' : '4 Episodes'
						},
						{
							'title': 'The Magdalene Sisters',
							'meta' : '1 Episodes'
						},
					]
				},
				{
					'letter': 'n',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'o',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'p',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'q',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'r',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 's',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 't',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'u',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'v',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'w',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'x',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'y',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': 'z',
					'programmes': [
						'B',
						'B1',
						'B2'
					]
				},
				{
					'letter': '0-9',
					'programs': [
						'B',
						'B1',
						'B2'
					]
				}
			]
		}
	}
});
