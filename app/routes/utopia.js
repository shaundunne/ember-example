import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	model: function() {
		var arr = [
			{
				series: 1,
				episode: 1,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep1_selected.png',
				date: 'Tue 15 Jan 2013',
				metadata: 's1e1_episode_metadata.png',
				episodes: 's1e1_clips_shorts.png'
			},
			{
				series: 1,
				episode: 2,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep2_selected.png',
				date: 'Tue 22 Jan 2013',
				metadata: 's1e2_episode_metadata.png',
				episodes: 's1e2_clips_shorts.png'
			},
			{
				series: 1,
				episode: 3,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep3_selected.png',
				date: 'Tue 29 Jan 2013',
				metadata: 's1e3_episode_metadata.png',
				episodes: 's1e3_clips_shorts.png'
			},
			{
				series: 1,
				episode: 4,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep4_selected.png',
				date: 'Tue 5 Feb 2013',
				metadata: 's1e4_episode_metadata.png',
				episodes: 's1e4_clips_shorts.png'
			},
			{
				series: 1,
				episode: 5,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep5_selected.png',
				date: 'Tue 12 Feb 2013',
				metadata: 's1e5_episode_metadata.png',
				episodes: 's1e5_clips_shorts.png'
			},
			{
				series: 1,
				episode: 6,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s1ep6_selected.png',
				date: 'Tue 19 Feb 2013',
				metadata: 's1e6_episode_metadata.png',
				episodes: 's1e6_clips_shorts.png'
			},
			{
				series: 2,
				episode: 1,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep1_selected.png',
				date: 'Mon 14 Jul 2014',
				metadata: 's2e1_episode_metadata.png',
				episodes: 's2e1_clips_shorts.png'
			},
			{
				series: 2,
				episode: 2,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep2_selected.png',
				date: 'Tue 15 Jul 2014',
				metadata: 's2e2_episode_metadata.png',
				episodes: 's2e2_clips_shorts.png'
			},
			{
				series: 2,
				episode: 3,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep3_selected.png',
				date: 'Tue 22 Jul 2014',
				metadata: 's2e3_episode_metadata.png',
				episodes: 's2e3_clips_shorts.png'
			},
			{
				series: 2,
				episode: 4,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep4_selected.png',
				date: 'Tue 29 Jul 2014',
				metadata: 's2e4_episode_metadata.png',
				episodes: 's2e4_clips_shorts.png'
			},
			{
				series: 2,
				episode: 5,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep5_selected.png',
				date: 'Tue 5 Aug 2014',
				metadata: 's2e5_episode_metadata.png',
				episodes: 's2e5_clips_shorts.png'
			},
			{
				series: 2,
				episode: 6,
				thumbnail: '/assets/img/utopia-hub/btn_carousel_episode_s2ep6_selected.png',
				date: 'Tue 12 Aug 2014',
				metadata: 's2e6_episode_metadata.png',
				episodes: 's2e6_clips_shorts.png'
			}
		];

		if ( Modernizr.mq('screen and (min-width : 1000px)') ) {
			arr.push( {}, {} );
		};
		return arr;
	},
	setupController: function(controller, model) {
		var user = this.controllerFor('application').get('user');
		controller.set('model', model);
		controller.set('user', user);
	},
	actions: {
		gotoPlayer: function() {
			this.transitionTo('utopia-player');
		},
		seriesItemClicked: function(index) {
			var controller = this.controllerFor('utopia');
			controller.trigger('selectSeries', index);
		}
	}
});