import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
    activate: function() {
        this._super.apply(this, arguments);
    },
	controllerName: 'player-landscape',
    renderTemplate: function() {
        this.render({ outlet: 'player' });
    },
    actions: {
    	didTransition: function() {
    		//console.log('reset me');
			this.controllerFor('mic-player-landscape').send('resetPlayer');
		}
    }
});
