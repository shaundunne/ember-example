import Ember from 'ember';
import ResetScroll from '../mixins/reset-scroll';

export default Ember.Route.extend(ResetScroll, {
	activate: function() {
		this._super.apply(this, arguments);
	},
	beforeModel: function() {
		this.controllerFor('application').checkUserSession();
	},
	actions: {
		didTransition: function() {
			this.controllerFor('application').send('resetDropdown');
		},
		goBack: function() {
			var applicationController = this.controllerFor('application');
			var previousPath = applicationController.get('previousPath');

			if (previousPath !== '') {

				if (previousPath === 'mic-player-landscape' || previousPath === 'utopia-player-landscape') {
					this.transitionTo('/');
				} else {
					this.transitionTo(previousPath);
				}
			}
		},
		goToLink: function(path){
			this.transitionTo(path);
		},
		userSessionDestroyed: function() {
			this.transitionTo('/');
		},
		userModified: function() {
			//Note: This action was handled by myfour route (if active)
		},
		goToSettings: function() {
			this.transitionTo('myfour.settings');
		},
		createNotification: function() {
			var applicationController = this.controllerFor('application');
			applicationController.createNotification();
		},
		markNotificationAsRead: function() {
			var applicationController = this.controllerFor('application');
			applicationController.markNotificationAsRead();
		}
	}
});
