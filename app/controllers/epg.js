import Ember from 'ember';
export default Ember.ObjectController.extend({
    isCatchupCalendarExpanded: false,
    actions: {
        toggleCatchupCalendar: function() {
        	var isExpanded = this.get('isCatchupCalendarExpanded');
            this.set('isCatchupCalendarExpanded', !isExpanded);
        },
        resetCalendarDropdown: function() {
        	this.set('isCatchupCalendarExpanded', false);
        }
    }
});