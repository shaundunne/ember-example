import Ember from 'ember';
export default Ember.ObjectController.extend({
    isExpanded: false,
    actions: {
        toggleCalendar: function() {
            var isExpanded = this.get('isExpanded');
            this.set('isExpanded', !isExpanded);
        }
    }
});