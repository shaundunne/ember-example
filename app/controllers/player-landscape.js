import Ember from 'ember';
export default Ember.ObjectController.extend({
    isPlaying: true,
    isEnded: false,
    actions: {
    	nextSlide: function() {
    		//console.log('Mobile-player: next slide');

            var isPlaying = this.get('isPlaying');

            if (isPlaying) {
                this.set('isPlaying', false);
                this.set('isEnded', true);
            } else {
                this.set('isEnded', false);
                this.send('goBack');
            }
    	},
    	resetPlayer: function() {
    		this.set('isPlaying', true);
            this.set('isEnded', false);
    	}
    }
});