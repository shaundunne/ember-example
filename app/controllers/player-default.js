import Ember from 'ember';
export default Ember.ObjectController.extend({
    isReady: true,
    isPlaying: false,
    isEnded: false,
    needs: ['application'],
    actions: {
    	nextSlide: function() {

            var user = this.get('controllers.application').get('user');
            if (!user) {
                var appController = this.get('controllers.application');
                appController.openLogin();
                return;
            }
            
            if( Modernizr.mq('screen and (min-width : 320px) and (max-width : 480px)') ) {
                console.log('launch landscape player');
                this.send('launchLandscapePlayer');
                return;
            }

    		if (this.get('isReady')) {
    			this.set('isReady', false);
    			this.set('isPlaying', true);
    		} 
    		else if (this.get('isPlaying')) {
    			this.set('isPlaying', false);
    			this.set('isEnded', true);
    		}
    		else {
    			this.set('isEnded', false);
                this.send('goBack');
    		}
    	},
    	resetPlayer: function() {
    		this.set('isReady', true);
    		this.set('isPlaying', false);
    		this.set('isEnded', false);
    	}
    }
});