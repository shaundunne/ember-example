import Ember from 'ember';
export default Ember.ArrayController.extend(Ember.Evented, {
    metadata: '',
    episodes: '',
    minSeries: 1,
    maxSeries: 2,
    seriesVal: 1,
    episodeIndex: 0,
    user: null,
    needs: ['application'],
    actions: {
    	currentSlideChanged: function(index) {

            /*var device = 'mobile';

            if( Modernizr.mq('screen and (min-width : 641px) and (max-width : 1224px)') ) {
                device = 'tablet';
            } else if ( Modernizr.mq('screen and (min-width : 1225px)') ) {
                device = 'desktop';
            }*/

            var device = 'desktop';

    		var item = this.get('model')[index];
            var folder = '/assets/img/utopia-hub/' + device + '/';
            this.set('metadata', folder + item.metadata);
            this.set('episodes', folder + item.episodes);
            this.set('seriesVal', item.series);
    	}
    },
    userTypeChanged: function() {
        var user = this.get('controllers.application').get('user');
        this.set('user', user);
    }.observes('controllers.application.user.type')
});