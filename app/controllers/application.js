import Ember from 'ember';
export default Ember.Controller.extend(Ember.Evented, {
	isExpanded: false,
	user: null,
	isLoginOpen: false,
	history: [],
	previousPath: '',
	currentPath: '',
	actions: {
		/*toggleDropdown: function() {
			alert('ApplicationController: toggleDropdown()');
			//var isOpen = this.get('isDropdownOpen');
            //this.set('isDropdownOpen', !isOpen);
		},*/
		resetDropdown: function() {
			console.log('ApplicationController: resetDropdown()');
			//alert('ApplicationController: resetDropdown()');
			//this.set('isDropdownOpen', false);
			this.set('isExpanded', false);
		},
		openLoginModal: function() {
			this.set('isLoginOpen', true);
			$('body').addClass('scroll-disabled');
		},
		closeLoginModal: function() {
			this.set('isLoginOpen', false);
			$('body').removeClass('scroll-disabled');
		},
		signMeIn: function() {
			var user = this.createUserRecord();
			this.set('user', user);
			this.send('closeLoginModal');
		},
		signMeOut: function() {
			var user = this.destroyUserRecord();
			this.set('user', user);
			this.send('closeLoginModal');
			this.send('userSessionDestroyed');
		}
	},
	onCurrentPath: function() {
		var currentPath = this.get('currentPath');
		var history = this.get('history');

		//add to the beginning of an array
		history.unshift(currentPath);

		if (history.length === 2) {
			this.set('previousPath', history[1]);
			history = history.slice(0, 1);
		}

		this.set('history', history);
		
	}.observes('currentPath'),
	checkUserSession: function() {
		//alert('checkUserSession');
		var self = this;
		this.store.findAll('user').then(function(record) {
			if (record.get('length') != 0) {
				var user = record.content[0];
				self.set('user', user);
			}
		})
	},
	createUserRecord: function() {
		var user = this.get('user');
		if (!user) {
			user = this.store.createRecord('user', {
				firstName: 'Matthew',
				lastName: 'Kay',
				type: 'first-time-use'
			});
			user.save();
		}
		return user;
	},
	destroyUserRecord: function() {
		this.store.findAll('user').then(function(record) {
			record.content.forEach(function(rec) {
				Ember.run.once(this, function() {
					rec.deleteRecord();
					rec.save();
				});
			}, this);
		});
		return null;
	},
	saveUserRecord: function() {
		var user = this.get('user');
		if (!user) {
			user.save();
		}
		return user;
	},
	openLogin: function() {
		this.send('openLoginModal');
	},
	createNotification: function() {
		var user = this.get('user');
		if (user) {
			user.set('type', 'notification-use');
			user.save();
		} else {
			this.openLogin();
		}
	},
	markNotificationAsRead:function() {
		var user = this.get('user');
		if (user) {
			user.set('type', 'regular-use');
			user.save();
		}
	}
});