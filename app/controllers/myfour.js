import Ember from 'ember';
export default Ember.Controller.extend({
    isSignedIn: false,
    isFirstTimeUse: false,
    hasNotification: false,
    needs: ['application'],
    isNotificationOpen: false,
    actions: {
        triggerNotificationsDropdown: function() {
            var val = this.get('isNotificationOpen');
            this.set('isNotificationOpen', !val);
        },
        closeNotificationsDropdown: function() {
            this.set('isNotificationOpen', false);
            this.send('markNotificationAsRead'); 
        }
    },
    invalidateUserState: function() {
        var user = this.get('model');
        if (user) {
            this.set('isSignedIn', true);

            var type = user.get('type');
            if (type === 'first-time-use') {
                this.set('isFirstTimeUse', true);
                this.set('hasNotification', false);
            } else if (type == 'notification-use') {
                this.set('isFirstTimeUse', false);
                this.set('hasNotification', true);
            } else {    //regular-use
                this.set('isFirstTimeUse', false);
                this.set('hasNotification', false)
            }
        } else {
            this.set('isSignedIn', false);
            this.set('isFirstTimeUse', false);
            this.set('hasNotification', false);
        }
    },
    userTypeChanged: function() {
        var user = this.get('controllers.application').get('user');
        this.set('model', user);
        this.invalidateUserState();
    }.observes('controllers.application.user.type')
});